//
//  ServiceManager.swift
//  NY High Schools SAT Avg
//
//  Created by Consultant on 6/7/22.
//

import Foundation

protocol DataFetcher {
    func fetchHighSchoolList(url: URL?, completion: @escaping (Result<Welcome, Error>) -> Void)
   
}

class NetworkManager {
    
    let session: Session
    
    init(session: Session = URLSession.shared) {
        self.session = session
    }
    
}

extension NetworkManager: DataFetcher {
    func fetchHighSchoolList(url: URL?, completion: @escaping (Result<Welcome, Error>) -> Void){
        
        guard let url = url else {
            completion(.failure(NetworkError.badURL))
            return
        }
        
        self.session.getData(url: url){ data, response, error in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let data = data else {
                completion(.failure(NetworkError.badData))
                return
            }

            do {
                let model = try JSONDecoder().decode(Welcome.self, from: data)
                completion(.success(model))
            } catch {
                completion(.failure(NetworkError.decodeError(error.localizedDescription)))
            }
        }
        
    }
}

