//
//  ViewController.swift
//  NY High Schools SAT Avg
//
//  Created by Consultant on 6/7/22.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var highSchoolTableView: UITableView = {
        let table = UITableView(frame: .zero)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.backgroundColor = .cyan
        table.dataSource = self
        table.delegate = self
        table.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.reuseId)
        return table
    }()
    
    var viewModel: UserAccess?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.setUpUI()
        self.viewModel = ViewControllerModel()
        self.viewModel?.bind { [weak self] in
            DispatchQueue.main.async {
                self?.highSchoolTableView.reloadData()
            }
        }
    }
    
    private func setUpUI() {
        self.title = "List of NY High Schools"
        self.view.backgroundColor = .white
        self.view.addSubview(self.highSchoolTableView)
        self.highSchoolTableView.bindToSuperView(insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        
    }

}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.numberOfHighSchools() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.reuseId, for: indexPath) as? TableViewCell else {
            return UITableViewCell()
        }
        
        //Sample Pics because API Didnt Have Picture
        if indexPath.row % 4 == 0{
            cell.sampleHSImageView.image = UIImage(named: "Sample1")
        }
        else if indexPath.row % 4 == 1{
            cell.sampleHSImageView.image = UIImage(named: "Sample2")
        }
        else if indexPath.row % 4 == 2{
            cell.sampleHSImageView.image = UIImage(named: "Sample4")
        }
        else{
            cell.sampleHSImageView.image = UIImage(named: "Sample5")
        }
        
        cell.hsName.text = self.viewModel?.nameOfHighSchool(index: indexPath.row)
    
        return cell
    }
    
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let DVC = DetailViewController()
        let hsinfo = self.viewModel?.hsInfo(index: indexPath.row)
        guard let dbn = hsinfo?.dbn else {
            return
        }
        guard let schoolname = hsinfo?.schoolName else {
            return
        }
        guard let numofTestTakers = hsinfo?.numOfSatTestTakers else {
            return
        }
        guard let avgReadingScore = hsinfo?.satCriticalReadingAvgScore else {
            return
        }
        guard let avgMathScore = hsinfo?.satMathAvgScore else {
            return
        }
        guard let avgWritingScore = hsinfo?.satWritingAvgScore else {
            return
        }
        
        DVC.dbnLabel.text = "School DBN number: " + dbn
        DVC.schoolNameLabel.text = "Name of HS: " + schoolname
        DVC.numofTestTakerLabel.text = "Number of SAT Takers: " + numofTestTakers
        DVC.readingScoreAvgLabel.text = "Average Reading Scores: " + avgReadingScore
        DVC.mathScoreAvgLabel.text = "Average Math Scores: " + avgMathScore
        DVC.writingScoreAvgLabel.text = "Average Writing Scores: " + avgWritingScore
        
        self.navigationController?.pushViewController(DVC, animated: true)
                
    }
}
