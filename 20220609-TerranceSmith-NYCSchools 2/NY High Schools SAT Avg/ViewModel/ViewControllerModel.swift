//
//  ViewControllerModel.swift
//  NY High Schools SAT Avg
//
//  Created by Consultant on 6/7/22.
//

import Foundation
import UIKit

protocol UserAccess{
    func bind(updateHandler: @escaping () -> Void)
    func returnHSList() -> Welcome
    func numberOfHighSchools() -> Int
    func nameOfHighSchool(index: Int) -> String
    func hsInfo(index: Int) -> WelcomeElement
}

class ViewControllerModel {
    var hsList: Welcome {
        didSet {
            self.updateHandler?()
        }
    }
    var updateHandler: (() -> Void)?
    let network: DataFetcher
    
    init(list: Welcome = [], network: DataFetcher = NetworkManager()){
        self.network = network
        self.hsList = list
        self.network.fetchHighSchoolList(url: NetworkParams.HighSchoolList.url){
            [weak self] (result: Result<Welcome, Error>) in
            switch result {
            case .success(let list):
                self?.hsList.append(contentsOf: list)
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension ViewControllerModel: UserAccess{
    
    func bind(updateHandler: @escaping () -> Void) {
        self.updateHandler = updateHandler
    }
    
    func returnHSList() -> Welcome{
        return self.hsList
    }
    
    func numberOfHighSchools() -> Int{
        return self.hsList.count
    }
   
    func nameOfHighSchool(index: Int) -> String{
        guard let hsName = self.hsList[index].schoolName else{
            return "Do Not Exist"
        }
        return hsName
    }
    
    func hsInfo(index: Int) -> WelcomeElement{
        return self.hsList[index]
    }
}
